package com.restaurant.backoffice.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.time.Instant;

import static org.junit.Assert.assertEquals;

public class EpochMilliDeserializerTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {

    }

    @Test
    public void deserialize() throws IOException {
        Instant now = Instant.now();
        String json = objectMapper.writeValueAsString(
                Restaurant.builder()
                        .id(1l)
                        .name("Restaurant 1")
                        .updatedAt(now.toEpochMilli())
                        .build()
        );
        Restaurant r = objectMapper.readValue(json, Restaurant.class);
        assertEquals(
                EpochMilliSerializer.DATE_TIME_FORMATTER.format(now),
                EpochMilliSerializer.DATE_TIME_FORMATTER.format(Instant.ofEpochMilli(r.getUpdatedAt()))
        );
    }

}
