package com.restaurant.backoffice.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class EpochMilliSerializerTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {

    }

    @Test
    public void serialize() throws JsonProcessingException {
        Instant now = Instant.now();
        String json = objectMapper.writeValueAsString(
                Restaurant.builder()
                        .id(1l)
                        .name("Restaurant 1")
                        .updatedAt(now.toEpochMilli())
                        .build()
        );
        assertThat(json, hasJsonPath("$.updatedAt",
                equalTo(EpochMilliSerializer.DATE_TIME_FORMATTER.format(now))));
    }

}
