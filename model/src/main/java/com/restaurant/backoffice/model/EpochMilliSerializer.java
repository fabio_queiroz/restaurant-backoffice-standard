package com.restaurant.backoffice.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Serializer to transform Epoch millis to a formatted date time, e.g.: yyyy-MM-dd HH:mm:ss
 */
public class EpochMilliSerializer extends JsonSerializer<Long> {

    /** Date time formatter */
    public static final DateTimeFormatter DATE_TIME_FORMATTER =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault());

    /**
     * Serialize Epoch millis
     *
     * @param value Long value
     * @param jg Json Generator
     * @param sp SerializerProvider
     * @throws IOException
     */
    @Override
    public void serialize(Long value, JsonGenerator jg, SerializerProvider sp) throws IOException {
        if (value == null) {
            jg.writeNull();
        } else {
            jg.writeString(DATE_TIME_FORMATTER.format(Instant.ofEpochMilli(value)));
        }
    }
}
