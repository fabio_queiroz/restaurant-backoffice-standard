package com.restaurant.backoffice.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.io.Serializable;

/**
 * Menu item model
 */
@ApiModel(
        value="MenuItem",
        description="Menu Item Entity model"
)
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuItem implements Serializable {

    /** Menu Item id */
    @ApiModelProperty("Menu Item id")
    private Long id;

    /** Menu Item name */
    @ApiModelProperty("Menu Item name")
    @NotEmpty(message = "Menu Item name is required")
    private String name;

    /** Menu Item price */
    @ApiModelProperty("Menu Item price")
    @Positive(message = "Menu Item price must be positive")
    private double price;

    /** Last update instant */
    @ApiModelProperty("Menu Item last update instant")
    @JsonSerialize(using = EpochMilliSerializer.class)
    @JsonDeserialize(using = EpochMilliDeserializer.class)
    private Long updatedAt;

}
