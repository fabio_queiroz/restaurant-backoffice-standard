package com.restaurant.backoffice.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * Restaurant model
 */
@ApiModel(
        value="Restaurant",
        description="Restaurant Entity model"
)
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Restaurant implements Serializable {

    public static final String CACHE_NAME = "Restaurants";

    /** Id */
    @ApiModelProperty("Restaurant id")
    private Long id;

    /** Name */
    @ApiModelProperty("Restaurant name")
    @NotEmpty(message = "Restaurant name is required")
    private String name;

    /** Last update instant */
    @ApiModelProperty("Restaurant last update instant")
    @JsonSerialize(using = EpochMilliSerializer.class)
    @JsonDeserialize(using = EpochMilliDeserializer.class)
    private Long updatedAt;

}
