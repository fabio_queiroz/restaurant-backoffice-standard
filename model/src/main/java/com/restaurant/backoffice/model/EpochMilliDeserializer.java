package com.restaurant.backoffice.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Deserializer to transform formatted date time to Epoch millis (Long)
 */
public class EpochMilliDeserializer extends JsonDeserializer<Long> {

    /**
     * Deserialize formatted date time
     *
     * @param jsonParser Json parser
     * @param context Deserialization context
     *
     * @return Epoch millis
     * @throws IOException
     */
    @Override
    public Long deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        return LocalDateTime.parse(jsonParser.getText(), EpochMilliSerializer.DATE_TIME_FORMATTER)
                .atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

}
