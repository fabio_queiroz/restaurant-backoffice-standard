package com.restaurant.backoffice.restapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Entity not found exception
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RestApiBaseException {

	public EntityNotFoundException(String message) {
		super(message);
	}

}