package com.restaurant.backoffice.restapi;

import com.restaurant.backoffice.restapi.exception.RestApiBaseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.Instant;
import java.util.Optional;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class RestApiExceptionHandler {

    /**
     * Handle Exception
     * @param e Exception
     * @return Error response
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        log.error("Error processing request", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                ErrorResponse.builder()
                    .errorMessage("Error processing this request")
                    .timestamp(Instant.now()
                ).build()
        );
    }

    /**
     * Handle MethodArgumentNotValidException
     * @param e MethodArgumentNotValidException
     * @return Error response
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody
    ErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error("Error validating request", e);
        return ErrorResponse.builder().errorMessage(
                e.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage)
                        .collect(Collectors.joining(",")))
                .timestamp(Instant.now())
                .build();
    }

    /**
     * Handle RestApiBaseException
     * @param e RestApiBaseException
     * @return Error response
     */
    @ExceptionHandler(RestApiBaseException.class)
    public ResponseEntity<ErrorResponse> handleRestApiBaseException(RestApiBaseException e) {
        log.error("Error processing request", e);
        return ResponseEntity.status(
                Optional.ofNullable(e.getClass().getDeclaredAnnotation(ResponseStatus.class))
                        .map(ResponseStatus::value).orElse(HttpStatus.INTERNAL_SERVER_ERROR)
        ).body(ErrorResponse.builder()
                .errorMessage(e.getMessage())
                .timestamp(Instant.now())
                .build()
        );
    }

}