package com.restaurant.backoffice.restapi;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.restaurant.backoffice.common.DateTimeHelper;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Instant;

/**
 * Jackson configuration for RestApi module
 */
@Configuration
public class JacksonConfiguration {

    /** Object mapper */
    private final ObjectMapper objectMapper;

    public JacksonConfiguration(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    /**
     * Initializer method responsible for module registration
     */
    @PostConstruct
    public void init() {
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new SimpleModule().addSerializer(Instant.class, new InstantSerializer()));
    }

    /**
     * Serializer for Instant objects
     */
    static class InstantSerializer extends JsonSerializer<Instant> {
        @Override
        public void serialize(Instant value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(DateTimeHelper.DTF_YYYY_MM_DD_HH_MM_SS.format(value));
        }

    }

}
