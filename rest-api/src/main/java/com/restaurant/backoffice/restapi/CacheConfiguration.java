package com.restaurant.backoffice.restapi;

import com.restaurant.backoffice.cache.event.CacheEventListener;
import com.restaurant.backoffice.model.Restaurant;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Cache configuration
 */
@Configuration
public class CacheConfiguration {

    /**
     * Defining restaurant cache event listener]
     *
     * @param cacheManager Cache manager
     *
     * @return Restaurant cache event listener
     */
    @Bean
    public CacheEventListener restaurantCacheEventListener(CacheManager cacheManager) {
        return new CacheEventListener(cacheManager, Restaurant.CACHE_NAME);
    }

}
