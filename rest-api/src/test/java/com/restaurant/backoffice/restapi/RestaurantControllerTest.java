package com.restaurant.backoffice.restapi;

import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.cache.event.CacheEventPublisher;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;
import com.restaurant.backoffice.datastore.entity.RestaurantEntity;
import com.restaurant.backoffice.datastore.repository.MenuItemRepository;
import com.restaurant.backoffice.datastore.repository.RestaurantRepository;
import com.restaurant.backoffice.model.MenuItem;
import com.restaurant.backoffice.model.Restaurant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.restaurant.backoffice.datastore.entity.RestaurantEntity.RestaurantFields;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
public class RestaurantControllerTest extends AbstractRestApiControllerTest {

    private static final StructuredQuery.OrderBy orderByName =
            StructuredQuery.OrderBy.asc(RestaurantFields.name.name());

    private static final String CONTROLLER_PATH = "/restaurants";

	@MockBean
    private RestaurantRepository restaurantRepository;

    @MockBean
	private MenuItemRepository menuItemRepository;

    @MockBean
    private CacheEventPublisher cacheEventPublisher;

    @Override
    public void before() {

    }

    @Override
    public void after() {

    }

    @Override
    protected Object createController() {
        return new RestaurantController(
                restaurantRepository,
                menuItemRepository,
                cacheEventPublisher
        );
    }

    @Override
    protected String getControllerPath() {
        return CONTROLLER_PATH;
    }

    @Test
	public void findRestaurantById_Restaurant_Not_Found() throws Exception {
		Long id = 1l;
        when(restaurantRepository.findById(id)).thenReturn(Optional.empty());
        getRequest("/" + id)
                .andExpect(status().isNotFound())
				.andExpect(content().string(containsString(String.format("Restaurant %s not found", id))));
        verify(restaurantRepository, times(1)).findById(id);
	}

    @Test
    public void findRestaurantById_Restaurant_Found() throws Exception {
        Long id = 1l;
        Restaurant restaurant = Restaurant.builder()
                .id(id)
                .name("Restaurant 1")
                .build();

        RestaurantEntity restaurantEntity = RestaurantEntity.builder()
                .id(id)
                .name(restaurant.getName())
                .build();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurantEntity));

        assertEquals(
                restaurant,
                objectMapper.readValue(getRequest("/" + id)
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse().
                    getContentAsString(), Restaurant.class)
        );
        verify(restaurantRepository, times(1)).findById(id);
    }

    @Test
    public void findAllRestaurants() throws Exception {
        Long id = 1l;
        List<RestaurantEntity> restaurants = new ArrayList<>();
        restaurants.add(
                RestaurantEntity.builder()
                        .id(1l)
                        .name("Restaurant 1")
                        .build()
        );
        restaurants.add(
                RestaurantEntity.builder()
                        .id(2l)
                        .name("Restaurant 2")
                        .build()
        );
        when(restaurantRepository.findAll(orderByName)).thenReturn(restaurants);
        assertEquals(
                objectMapper.writeValueAsString(
                        restaurants.stream().map(
                                re -> Restaurant.builder()
                                        .id(re.getId())
                                        .name(re.getName())
                                        .build()
                        ).collect(Collectors.toList())
                ),
                getRequest("")
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse().
                        getContentAsString()
        );
        verify(restaurantRepository, times(1)).findAll(orderByName);
    }

    @Test
    public void createRestaurant_Invalid_Request() throws Exception {
        postRequest("", Restaurant.builder().build()).andExpect(status().isBadRequest());
    }

    @Test
    public void createRestaurant() throws Exception {
        Long id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(id)
                .name("Restaurant 1")
                .build();
        RestaurantEntity restaurant2 = RestaurantEntity.builder()
                .name(restaurant.getName())
                .build();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));
        when(restaurantRepository.save(
                restaurant2
        )).thenReturn(id);
        Restaurant restaurant3 = objectMapper.readValue(postRequest("", restaurant2)
                .andExpect(status().isCreated()).andReturn()
                .getResponse().getContentAsString(), Restaurant.class);
        verify(restaurantRepository, times(1)).save(restaurant2);
        assertEquals(
                id,
                restaurant3.getId()
        );
    }

    @Test
    public void updateRestaurant_Invalid_Request() throws Exception {
        long id = 1l;
        putRequest("/" + id, Restaurant.builder().build()).andExpect(status().isBadRequest());
    }

    @Test
    public void updateRestaurant_Restaurant_Not_Found() throws Exception {
        long id = 1l;
        putRequest("/" + id,
                Restaurant.builder()
                        .name("Restaurant Test")
                        .build()
        ).andExpect(status().isNotFound())
        .andExpect(content().string(containsString(String.format("Restaurant %s not found", id))));
    }

    @Test
    public void updateRestaurant() throws Exception {
        long id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(id)
                .name("Restaurant Test")
                .build();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));
        putRequest("/" + id,
                Restaurant.builder()
                        .name("Restaurant Test")
                        .build()
        ).andExpect(status().isOk());
        verify(restaurantRepository, times(1)).save(restaurant);
        verify(cacheEventPublisher, times(1))
                .publishPutEvent(controller, id, Restaurant.builder()
                        .id(id)
                        .name("Restaurant Test")
                        .build()
                );
    }

    @Test
    public void deleteRestaurant() throws Exception {
        long id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(id)
                .name("Restaurant Test")
                .build();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));
        deleteRequest("/" + id).andExpect(status().isOk());
        verify(restaurantRepository, times(1)).delete(id);
        verify(cacheEventPublisher, times(1)).publishRemoveEvent(
                controller,
                id,
                Restaurant.builder()
                        .id(id)
                        .name("Restaurant Test")
                        .build()
        );
    }

    @Test
    public void findMenuItemByIdAndRestaurantId_Restaurant_Not_Found() throws Exception {
        Long restaurantId = 1l;
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.empty());
        getRequest(String.format("/%s/menuitems/%s", restaurantId, 2l))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(String.format("Restaurant %s not found", restaurantId))));
        verify(restaurantRepository, times(1)).findById(restaurantId);
    }

    @Test
    public void findMenuItemByIdAndRestaurantId_MenuItem_Not_Found() throws Exception {
        long restaurantId = 1l, id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant Test")
                .updatedAt(Instant.now().toEpochMilli())
                .build();
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.of(restaurant));
        when(menuItemRepository.findById(restaurantId)).thenReturn(Optional.empty());
        getRequest(String.format("/%s/menuitems/%s", restaurantId, id))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(
                        String.format("Menu Item %s not found for Restaurant %s", id, restaurantId))));
        verify(restaurantRepository, times(1)).findById(restaurantId);
    }

    @Test
    public void findMenuItemByIdAndRestaurantId() throws Exception {
        long restaurantId = 1l, id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant 1")
                .build();
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.of(restaurant));
        MenuItemEntity menuItem = MenuItemEntity.builder()
                .id(id)
                .name("Rocket Burger")
                .price(30.50)
                .restaurantId(restaurantId)
                .build();
        when(menuItemRepository.findById(id)).thenReturn(Optional.of(menuItem));

        MenuItem menuItem2 = objectMapper.readValue(
                getRequest(String.format("/%s/menuitems/%s", restaurantId, id))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                MenuItem.class
        );
        assertEquals(
                MenuItem.builder()
                        .id(menuItem.getId())
                        .name(menuItem.getName())
                        .price(menuItem.getPrice())
                        .updatedAt(menuItem.getUpdatedAt())
                        .build(),
                menuItem2
        );
        verify(restaurantRepository, times(1)).findById(id);
        verify(menuItemRepository, times(1))
                .findById(id);
    }

    @Test
    public void findAllMenuItemsByRestaurantId_Restaurant_Not_Found() throws Exception {
        long restaurantId = 1l;
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.empty());
        getRequest(
                String.format("/%s/menuitems", restaurantId)
        ).andExpect(status().isNotFound())
                .andExpect(content().string(containsString(String.format("Restaurant %s not found", restaurantId))));
        verify(restaurantRepository, times(1)).findById(restaurantId);
    }

    @Test
    public void findAllMenuItemsByRestaurantId() throws Exception {
        long restaurantId = 1l, id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant 1")
                .build();
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.of(restaurant));
        MenuItemEntity menuItemEntity = MenuItemEntity.builder()
                .id(id)
                .name("Rocket Burger")
                .price(30.50)
                .restaurantId(restaurantId)
                .build();
        MenuItem menuItem = MenuItem.builder()
                .id(id)
                .name(menuItemEntity.getName())
                .price(menuItemEntity.getPrice())
                .build();

        when(menuItemRepository.findAllByRestaurantId(restaurantId, orderByName)).thenReturn(
                Arrays.asList(menuItemEntity));
        assertEquals(
                objectMapper.writeValueAsString(Arrays.asList(menuItem)),
                getRequest(String.format("/%s/menuitems", restaurantId))
                        .andExpect(status().isOk())
                        .andReturn()
                        .getResponse().
                        getContentAsString()
        );
        verify(menuItemRepository, times(1)).findAllByRestaurantId(restaurantId, orderByName);
    }

    @Test
    public void createMenuItem_Invalid_Request_Body() throws Exception {
        Long restaurantId = 1l;
        postRequest(
                String.format("/%s/menuitems", restaurantId),
                MenuItem.builder().build()
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void createMenuItem_Restaurant_Not_Found() throws Exception {
        long restaurantId = 1l, id = 1l;
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.empty());
        postRequest(
                String.format("/%s/menuitems", restaurantId),
                MenuItem.builder()
                        .id(id)
                        .name("Rocket Burger")
                        .price(30.50)
                        .build()
        ).andExpect(status().isNotFound())
                .andExpect(content().string(containsString(String.format("Restaurant %s not found", restaurantId))));
        verify(restaurantRepository, times(1)).findById(restaurantId);
    }

    @Test
    public void createMenuItem() throws Exception {
        long restaurantId = 1l, id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant 1")
                .build();
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.of(restaurant));
        MenuItemEntity menuItem = MenuItemEntity.builder()
                .name("Rocket Burger")
                .price(30.50)
                .restaurantId(restaurantId)
                .build();
        when(menuItemRepository.findById(id)).thenReturn(Optional.of(menuItem));
        when(menuItemRepository.save(menuItem)).thenReturn(id);
        postRequest(String.format("/%s/menuitems", restaurantId), menuItem).andExpect(status().isCreated());
        verify(menuItemRepository, times(1)).save(menuItem);
        verify(menuItemRepository, times(1)).findById(id);
    }

    @Test
    public void updateMenuItem_Restaurant_Not_Found() throws Exception {
        Long restaurantId = 1l;
        Long id = 1l;
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.empty());
        MenuItemEntity menuItem = MenuItemEntity.builder()
                .id(id)
                .name("Rocket Burger")
                .price(30.50)
                .restaurantId(restaurantId)
                .build();
        putRequest(String.format("/%s/menuitems/%s", restaurantId, id), menuItem)
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(String.format("Restaurant %s not found", restaurantId))));
        verify(restaurantRepository, times(1)).findById(restaurantId);
    }

    @Test
    public void updateMenuItem_MenuItem_Not_Found() throws Exception {
        Long restaurantId = 1l;
        Long id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant 1")
                .updatedAt(Instant.now().toEpochMilli())
                .build();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));
        when(menuItemRepository.findById(id)).thenReturn(Optional.empty());
        MenuItemEntity menuItem = MenuItemEntity.builder()
                .id(id)
                .name("Rocket Burger")
                .price(30.50)
                .restaurantId(restaurantId)
                .build();
        putRequest(String.format("/%s/menuitems/%s", restaurantId, id), menuItem)
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(String.format("Menu Item %s not found for Restaurant %s", id, restaurantId))));
        verify(restaurantRepository, times(1)).findById(id);
        verify(menuItemRepository, times(1)).findById(id);
    }

    @Test
    public void updateMenuItem() throws Exception {
        long restaurantId = 1l, id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant 1")
                .build();
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.of(restaurant));
        MenuItemEntity menuItem = MenuItemEntity.builder()
                .id(id)
                .name("Rocket Burger")
                .price(30.50)
                .restaurantId(restaurantId)
                .build();
        when(menuItemRepository.findById(id)).thenReturn(Optional.of(menuItem));
        when(menuItemRepository.save(menuItem)).thenReturn(id);
        putRequest(String.format("/%s/menuitems/%s", restaurantId, id), menuItem).andExpect(status().isOk());
        verify(menuItemRepository, times(1)).save(menuItem);
        verify(menuItemRepository, times(1)).findById(id);
    }

    @Test
    public void deleteMenuItem_Restaurant_Not_Found() throws Exception {
        Long restaurantId = 1l;
        Long id = 1l;
        when(restaurantRepository.findById(restaurantId)).thenReturn(Optional.empty());
        deleteRequest(String.format("/%s/menuitems/%s", restaurantId, id))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(String.format("Restaurant %s not found", restaurantId))));
        verify(restaurantRepository, times(1)).findById(restaurantId);
    }

    @Test
    public void deleteMenuItem_MenuItem_Not_Found() throws Exception {
        Long restaurantId = 1l;
        Long id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant 1")
                .build();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));
        when(menuItemRepository.findById(id)).thenReturn(Optional.empty());
        deleteRequest(String.format("/%s/menuitems/%s", restaurantId, id))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(String.format("Menu Item %s not found for Restaurant %s", id, restaurantId))));
        verify(restaurantRepository, times(1)).findById(id);
        verify(menuItemRepository, times(1)).findById(id);
    }

    @Test
    public void deleteMenuItem() throws Exception {
        Long restaurantId = 1l;
        Long id = 1l;
        RestaurantEntity restaurant = RestaurantEntity.builder()
                .id(restaurantId)
                .name("Restaurant 1")
                .build();
        when(restaurantRepository.findById(id)).thenReturn(Optional.of(restaurant));
        MenuItemEntity menuItem = MenuItemEntity.builder()
                .id(id)
                .name("Rocket Burger")
                .price(30.50)
                .restaurantId(restaurantId)
                .build();
        when(menuItemRepository.findById(id)).thenReturn(Optional.of(menuItem));
        deleteRequest(String.format("/%s/menuitems/%s", restaurantId, id))
                .andExpect(status().isOk());
        verify(restaurantRepository, times(1)).findById(id);
        verify(menuItemRepository, times(1)).findById(id);
        verify(menuItemRepository, times(1)).delete(id);
    }

}
