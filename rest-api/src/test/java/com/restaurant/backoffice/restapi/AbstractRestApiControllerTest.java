package com.restaurant.backoffice.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.After;
import org.junit.Before;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

public abstract class AbstractRestApiControllerTest {

    protected Object controller;

    protected MockMvc mvc;

    protected ObjectMapper objectMapper = new ObjectMapper();

    protected abstract Object createController();

    protected abstract String getControllerPath();

    protected abstract void before();

    protected abstract void after();

    @Before
    public void setUp() {
        // MockMvc standalone approach
        controller = createController();
        mvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new RestApiExceptionHandler())
                .build();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        before();
    }

    @After
    public void tearDown() {
        after();
    }

	protected ResultActions getRequest(String path) throws Exception {
        return perform(get(getControllerPath().concat(path)));
    }

    protected ResultActions deleteRequest(String path) throws Exception {
        return perform(delete(getControllerPath().concat(path)));
    }

    protected ResultActions postRequest(String path, Object body) throws Exception {
        return perform(post(getControllerPath().concat(path)), body);
    }

    protected ResultActions putRequest(String path, Object body) throws Exception {
        return perform(put(getControllerPath().concat(path)), body);
    }

    protected ResultActions perform(MockHttpServletRequestBuilder builder) throws Exception {
        return this.mvc.perform(builder.contentType(MediaType.APPLICATION_JSON)).andDo(print());
    }

    protected ResultActions perform(MockHttpServletRequestBuilder builder, Object body) throws Exception {
        return this.mvc.perform(builder.contentType(MediaType.APPLICATION_JSON)
            .content(
                objectMapper.writeValueAsString(body)
            )
        ).andDo(print());
    }
}
