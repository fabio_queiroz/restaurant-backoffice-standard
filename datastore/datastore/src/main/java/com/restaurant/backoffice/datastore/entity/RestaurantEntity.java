package com.restaurant.backoffice.datastore.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import lombok.*;

/**
 * Restaurant entity
 */
@Entity(name = "Restaurant")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString(callSuper = true)
public class RestaurantEntity extends AbstractEntity {

    /** Field names enum */
    public enum RestaurantFields {
        id,
        name,
        updatedAt
    }

    /** Name */
    @Index
    private String name;

    /**
     * All arguments constructor
     *
     * @param id Id
     * @param name Name
     * @param updatedAt Updated at
     */
    @Builder
    public RestaurantEntity(Long id, String name, Long updatedAt) {
        super(id, updatedAt);
        this.name = name;
    }

}
