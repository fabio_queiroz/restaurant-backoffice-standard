package com.restaurant.backoffice.datastore.entity;

import com.googlecode.objectify.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Abstract entity
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AbstractEntity {

    /** Id */
    @Id
    private Long id;

    /** Last update instant */
    private Long updatedAt;

}
