package com.restaurant.backoffice.datastore.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;
import lombok.*;

/**
 * Menu item entity
 */
@Entity(name = "MenuItem")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString(callSuper = true)
public class MenuItemEntity extends AbstractEntity {

    /** Field names enum */
    public enum MenuItemFields {
        id,
        name,
        price,
        restaurantId,
        updatedAt
    }

    /** Name */
    @Index
    private String name;

    /** Price */
    @Index
    private double price;

    /** Restaurant id */
    @Index
    private Long restaurantId;

    /**
     * All arguments constructor
     *
     * @param id Id
     * @param name Name
     * @param price Price
     * @param restaurantId Restaurant Id
     * @param updatedAt Updated at
     */
    @Builder
    public MenuItemEntity(Long id, String name, double price, Long restaurantId, Long updatedAt) {
        super(id, updatedAt);
        this.name = name;
        this.price = price;
        this.restaurantId = restaurantId;
    }

}
