package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;

import java.util.List;

/**
 * Menu Item repository
 */
public interface MenuItemRepository extends EntityRepository<MenuItemEntity> {

    /**
     * Find all Menu Items of a Restaurant
     *
     * @param restaurantId Restaurant id
     *
     * @return List of Menu Items
     */
    List<MenuItemEntity> findAllByRestaurantId(long restaurantId);

    /**
     * Find all Menu Items of a Restaurant
     *
     * @param restaurantId Restaurant id
     * @param orderBy Order by
     *
     * @return List of Menu Items
     */
    List<MenuItemEntity> findAllByRestaurantId(long restaurantId, StructuredQuery.OrderBy orderBy);

}