package com.restaurant.backoffice.datastore.repository;

import com.restaurant.backoffice.datastore.entity.RestaurantEntity;

/**
 * Restaurant repository
 */
public interface RestaurantRepository extends EntityRepository<RestaurantEntity> {

}
