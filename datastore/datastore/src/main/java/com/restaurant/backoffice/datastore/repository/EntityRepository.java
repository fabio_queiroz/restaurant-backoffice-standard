package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.AbstractEntity;

import java.util.List;
import java.util.Optional;

/**
 * Entity repository
 */
public interface EntityRepository<T extends AbstractEntity> {

    /**
     * Find entity by id
     *
     * @param id Entity id
     * @return Entity
     */
    Optional<T> findById(long id);

    /**
     * Find all entities
     *
     * @return List of entities
     */
    List<T> findAll();

    /**
     * Find all entities sorted
     *
     * @param orderBy Order by field
     *
     * @return List of entities
     */
    List<T> findAll(StructuredQuery.OrderBy orderBy);

    /**
     * Save entity
     *
     * @param entity Entity
     * @return Entity id
     */
    long save(T entity);

    /**
     * Delete entity
     *
     * @param id Entity id
     */
    void delete(long id);

}
