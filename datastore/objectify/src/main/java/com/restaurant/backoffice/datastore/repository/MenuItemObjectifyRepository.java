package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.common.DateTimeHelper;
import com.restaurant.backoffice.datastore.ObjectifyServiceFactory;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;

import java.util.List;

import static com.restaurant.backoffice.datastore.entity.MenuItemEntity.MenuItemFields;

/**
 * Menu Item repository implementation using Objectify API
 */
public class MenuItemObjectifyRepository extends AbstractEntityObjectifyRepository<MenuItemEntity>
        implements MenuItemRepository {

    /** Date time helper */
    private final DateTimeHelper dateTimeHelper;

    /**
     * Required arguments constructor
     *
     * @param objectifyServiceFactory Objectify service factory
     * @param dateTimeHelper Date time helper
     */
    public MenuItemObjectifyRepository(
            ObjectifyServiceFactory objectifyServiceFactory,
            DateTimeHelper dateTimeHelper) {
        super(objectifyServiceFactory);
        this.dateTimeHelper = dateTimeHelper;
    }

    /**
     * Save a restaurant
     * Set updatedAt attribute with current instant
     *
     * @param menuItem Menu Item entity bean
     *
     * @return Menu Item id
     */
    @Override
    public long save(MenuItemEntity menuItem) {
        return super.save(
                MenuItemEntity.builder()
                        .id(menuItem.getId())
                        .name(menuItem.getName())
                        .price(menuItem.getPrice())
                        .restaurantId(menuItem.getRestaurantId())
                        .updatedAt(dateTimeHelper.nowEpochMilli())
                        .build()
        );
    }

    /**
     * Find all Menu Items of a Restaurant
     *
     * @return List of Menu Items
     */
    @Override
    public List<MenuItemEntity> findAllByRestaurantId(long restaurantId) {
        return loadByType().filter(MenuItemFields.restaurantId.name(), restaurantId).list();
    }

    /**
     * Find all Menu Items of a Restaurant sorted
     *
     * @param restaurantId Restaurant id
     * @param orderBy Order by
     *
     * @return List of Menu Items
     */
    @Override
    public List<MenuItemEntity> findAllByRestaurantId(long restaurantId, StructuredQuery.OrderBy orderBy) {
        return loadByType().filter(MenuItemFields.restaurantId.name(), restaurantId).order(orderBy.getProperty()).list();
    }

}
