package com.restaurant.backoffice.datastore;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * Objectify Factory needed to be possible to develop Unit Tests because ObjectifyService.ofy is static
 */
public class ObjectifyServiceFactory {

    /**
     * Get Objectify instance
     *
     * @return Objectify
     */
    public Objectify ofy() {
        return ObjectifyService.ofy();
    }

}
