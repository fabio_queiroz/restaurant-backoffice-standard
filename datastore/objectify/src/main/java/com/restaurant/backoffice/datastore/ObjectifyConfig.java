package com.restaurant.backoffice.datastore;


import com.googlecode.objectify.ObjectifyFilter;
import com.googlecode.objectify.ObjectifyService;
import com.restaurant.backoffice.common.DateTimeHelper;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;
import com.restaurant.backoffice.datastore.entity.RestaurantEntity;
import com.restaurant.backoffice.datastore.repository.MenuItemObjectifyRepository;
import com.restaurant.backoffice.datastore.repository.RestaurantObjectifyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;

/**
 * Objectify Configuration
 */
@Profile("objectify")
@Configuration
@Slf4j
public class ObjectifyConfig {

    /**
     * Initializing Objectify
     */
    @PostConstruct
    private void init() {
        log.info("Initializing Objectify");
        ObjectifyService.init();
        ObjectifyService.register(RestaurantEntity.class);
        ObjectifyService.register(MenuItemEntity.class);
    }

    /**
     * Defining Objectify Service Factory
     *
     * @return Objectify service factory
     */
    @Bean
    public ObjectifyServiceFactory objectifyFactory() {
        return new ObjectifyServiceFactory();
    }

    /**
     * Defining Objectify context filter
     *
     * @return Objectify filter
     */
    @Bean
    public FilterRegistrationBean<ObjectifyFilter> objectifyFilter() {
        FilterRegistrationBean<ObjectifyFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(new ObjectifyFilter());
        return filterRegistrationBean;
    }

    /**
     * Defining RestaurantObjectifyRepository bean
     *
     * @param objectifyFactory Objectify factory
     * @param menuItemObjectifyRepository Menu Item objectify repository
     * @param dateTimeHelper Date time helper
     *
     * @return RestaurantObjectifyRepository bean
     */
    @Bean
    public RestaurantObjectifyRepository restaurantObjectifyRepository(
            ObjectifyServiceFactory objectifyFactory,
            MenuItemObjectifyRepository menuItemObjectifyRepository,
            DateTimeHelper dateTimeHelper) {
        return new RestaurantObjectifyRepository(objectifyFactory, menuItemObjectifyRepository, dateTimeHelper);
    }

    /**
     * Defining MenuItemObjectifyRepository bean
     *
     * @param objectifyFactory Objectify factory
     * @param dateTimeHelper Date time helper
     *
     * @return MenuItemDatastoreRepository bean
     */
    @Bean
    public MenuItemObjectifyRepository menuItemObjectifyRepository(
            ObjectifyServiceFactory objectifyFactory,
            DateTimeHelper dateTimeHelper) {
        return new MenuItemObjectifyRepository(objectifyFactory, dateTimeHelper);
    }

}
