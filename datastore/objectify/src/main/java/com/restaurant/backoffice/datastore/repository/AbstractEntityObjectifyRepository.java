package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.StructuredQuery;
import com.googlecode.objectify.cmd.LoadType;
import com.restaurant.backoffice.datastore.ObjectifyServiceFactory;
import com.restaurant.backoffice.datastore.entity.AbstractEntity;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

/**
 * Abstract repository implementation using Objectify API
 */
public abstract class AbstractEntityObjectifyRepository<T extends AbstractEntity> implements EntityRepository<T> {

    /** Objectify service factory */
    private final ObjectifyServiceFactory objectifyServiceFactory;

    /** Kind name */
    protected final Class<T> type;

    /**
     * Default constructor
     *
     * @param objectifyServiceFactory Objectify service factory
     */
    public AbstractEntityObjectifyRepository(ObjectifyServiceFactory objectifyServiceFactory) {
        this.objectifyServiceFactory = objectifyServiceFactory;
        type = ((Class) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0]);
    }

    /**
     * Load by type
     *
     * @return Load type
     */
    protected LoadType<T> loadByType() {
        return objectifyServiceFactory.ofy().load().type(type);
    }

    /**
     * Find entity by id
     *
     * @param id Entity id
     * @return Entity
     */
    @Override
    public Optional<T> findById(long id) {
        return Optional.ofNullable(loadByType().id(id).now());
    }

    /**
     * Find all entities
     *
     * @return List of entities
     */
    @Override
    public List<T> findAll() {
        return loadByType().list();
    }

    /**
     * Find all entities sorted
     *
     * @param orderBy Order by
     *
     * @return List of entities
     */
    //TODO REVIEW ORDER BY OBJECTS
    @Override
    public List<T> findAll(StructuredQuery.OrderBy orderBy) {
        return loadByType().order(orderBy.getProperty()).list();
    }

    /**
     * Save a entity
     *
     * @param entityBean Entity bean
     * @return Entity id
     */
    @Override
    public long save(T entityBean) {
        return objectifyServiceFactory.ofy().save().entity(entityBean).now().getId();
    }

    /**
     * Delete entity
     *
     * @param id Entity id
     */
    @Override
    public void delete(long id) {
        objectifyServiceFactory.ofy().delete().type(type).id(id).now();
    }

}