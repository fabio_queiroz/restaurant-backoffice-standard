package com.restaurant.backoffice.datastore.repository;

import com.restaurant.backoffice.common.DateTimeHelper;
import com.restaurant.backoffice.datastore.ObjectifyServiceFactory;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;
import com.restaurant.backoffice.datastore.entity.RestaurantEntity;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

/**
 * Restaurant repository implementation using Objectify API
 */
@Slf4j
public class RestaurantObjectifyRepository extends AbstractEntityObjectifyRepository<RestaurantEntity>
        implements RestaurantRepository {

    /** Menu Item datastore repository */
    private final MenuItemObjectifyRepository menuItemObjectifyRepository;

    /** Date time helper */
    private final DateTimeHelper dateTimeHelper;

    /**
     * Required arguments constructor
     *
     * @param objectifyServiceFactory Objectify service factory
     * @param menuItemObjectifyRepository Menu Item repository
     * @param dateTimeHelper Date time helper
     */
    public RestaurantObjectifyRepository(
            ObjectifyServiceFactory objectifyServiceFactory,
            MenuItemObjectifyRepository menuItemObjectifyRepository,
            DateTimeHelper dateTimeHelper) {
        super(objectifyServiceFactory);
        this.menuItemObjectifyRepository = menuItemObjectifyRepository;
        this.dateTimeHelper = dateTimeHelper;
    }

    @Override
    public Optional<RestaurantEntity> findById(long id) {
        log.info("Finding restaurant by id {} on Datastore", id);
        return super.findById(id);
    }

    /**
     * Save a restaurant
     * Set updatedAt attribute with current instant
     *
     * @param restaurant Restaurant entity bean
     *
     * @return Restaurant id
     */
    @Override
    public long save(RestaurantEntity restaurant) {
        return super.save(
                RestaurantEntity.builder()
                        .id(restaurant.getId())
                        .name(restaurant.getName())
                        .updatedAt(dateTimeHelper.nowEpochMilli())
                        .build()
        );
    }

    /**
     * Delete restaurant and its menu items
     *
     * @param restaurantId Restaurant id
     */
    //TODO REVIEW ORDER BY OBJECT
    @Override
    public void delete(long restaurantId) {
        menuItemObjectifyRepository.findAllByRestaurantId(restaurantId).stream()
                .mapToLong(MenuItemEntity::getId).forEach(menuItemObjectifyRepository::delete);
        super.delete(restaurantId);
    }

}
