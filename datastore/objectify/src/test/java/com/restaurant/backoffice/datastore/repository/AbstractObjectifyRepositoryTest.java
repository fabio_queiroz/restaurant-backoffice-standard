package com.restaurant.backoffice.datastore.repository;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.cmd.LoadType;
import com.googlecode.objectify.cmd.Loader;
import com.restaurant.backoffice.common.DateTimeHelper;
import com.restaurant.backoffice.datastore.ObjectifyServiceFactory;
import com.restaurant.backoffice.datastore.entity.AbstractEntity;
import org.junit.After;
import org.junit.Before;

import java.time.Instant;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractObjectifyRepositoryTest {

    protected final Instant now = Instant.now();

    protected DateTimeHelper dateTimeHelper;

    protected ObjectifyServiceFactory objectifyServiceFactory;

    protected Objectify objectify;

    protected Loader loader;

    protected LoadType loadType;

    protected abstract void before();

    protected abstract void after();

    protected abstract Class<? extends AbstractEntity> getType();

    @Before
    public void setUp() {
        dateTimeHelper = mock(DateTimeHelper.class);
        objectifyServiceFactory = mock(ObjectifyServiceFactory.class);
        objectify = mock(Objectify.class);
        loader = mock(Loader.class);
        loadType = mock(LoadType.class);
        when(objectifyServiceFactory.ofy()).thenReturn(objectify);
        when(objectify.load()).thenReturn(loader);
        when(loader.type(getType())).thenReturn(loadType);
        when(dateTimeHelper.nowEpochMilli()).thenReturn(now.toEpochMilli());
        before();
    }

    @After
    public void tearDown() {
        after();
    }

}