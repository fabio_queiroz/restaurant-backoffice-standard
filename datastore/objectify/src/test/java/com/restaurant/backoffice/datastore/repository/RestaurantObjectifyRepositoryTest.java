package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.StructuredQuery;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.LoadResult;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.cmd.DeleteType;
import com.googlecode.objectify.cmd.Deleter;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.cmd.Saver;
import com.restaurant.backoffice.datastore.entity.AbstractEntity;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;
import com.restaurant.backoffice.datastore.entity.RestaurantEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.restaurant.backoffice.datastore.entity.RestaurantEntity.RestaurantFields;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantObjectifyRepositoryTest extends AbstractObjectifyRepositoryTest {

    private static final StructuredQuery.OrderBy orderByName =
            StructuredQuery.OrderBy.asc(RestaurantFields.name.name());

    private final long id1 = 1;
    private final long id2 = 2;

    private RestaurantEntity restaurant1;
    private RestaurantEntity restaurant2;

    private RestaurantObjectifyRepository restaurantRepository;

    @Mock
    private MenuItemObjectifyRepository menuItemRepository;

    @Mock
    private LoadResult loadResult1;

    @Mock
    private Key key1;

    @Override
    protected Class<? extends AbstractEntity> getType() {
        return RestaurantEntity.class;
    }

    public void before() {
        restaurantRepository = new RestaurantObjectifyRepository(objectifyServiceFactory, menuItemRepository, dateTimeHelper);
        restaurant1 = RestaurantEntity.builder()
                .id(id1)
                .name("Restaurant 1")
                .updatedAt(dateTimeHelper.nowEpochMilli())
                .build();
        restaurant2 = RestaurantEntity.builder()
                .id(id2)
                .name("Restaurant 2")
                .updatedAt(dateTimeHelper.nowEpochMilli())
                .build();
        when(loadType.id(id1)).thenReturn(loadResult1);
        when(key1.getId()).thenReturn(id1);
    }

    public void after() {

    }

    @Test
    public void findById_Not_Found() {
        Optional<RestaurantEntity> restaurant = restaurantRepository.findById(id1);
        assertFalse(restaurant.isPresent());
    }

    @Test
    public void findById() {
        when(loadResult1.now()).thenReturn(restaurant1);
        Optional<RestaurantEntity> restaurantOpt = restaurantRepository.findById(id1);
        verify(objectifyServiceFactory, times(1)).ofy();
        verify(loader, times(1)).type(getType());
        assertEquals(restaurant1, restaurantOpt.get());
    }

    @Test
    public void findAll() {
        List<RestaurantEntity> restaurants = Arrays.asList(restaurant2, restaurant1);
        when(loadType.list()).thenReturn(restaurants);
        List<RestaurantEntity> restaurants2 = restaurantRepository.findAll();
        assertEquals(restaurants, restaurants2);
        verify(loader, times(1)).type(getType());
        verify(loadType, times(1)).list();
    }

    @Test
    public void findAll_Sorted() {
        List<RestaurantEntity> restaurants = Arrays.asList(restaurant2, restaurant1);
        Query query = mock(Query.class);
        when(loadType.order(orderByName.getProperty())).thenReturn(query);
        when(query.list()).thenReturn(restaurants);
        List<RestaurantEntity> restaurants2 = restaurantRepository.findAll(orderByName);
        assertEquals(restaurants, restaurants2);
        verify(loader, times(1)).type(getType());
        verify(loadType, times(1)).order(orderByName.getProperty());
        verify(query, times(1)).list();
    }

    @Test
    public void save_Create() {
        long id3 = 3;
        RestaurantEntity restaurant3 = RestaurantEntity.builder()
                .name("Restaurant 3")
                .build();
        Saver saver = mock(Saver.class);
        when(objectify.save()).thenReturn(saver);
        Result result = mock(Result.class);
        when(saver.entity(
                RestaurantEntity.builder()
                .name(restaurant3.getName())
                .updatedAt(dateTimeHelper.nowEpochMilli())
                .build()
        )).thenReturn(result);
        Key key3 = mock(Key.class);
        when(key3.getId()).thenReturn(id3);
        when(result.now()).thenReturn(key3);
        long id = restaurantRepository.save(restaurant3);
        assertEquals(id3, id);
        verify(objectify, times(1)).save();
        verify(saver, times(1)).entity(
                RestaurantEntity.builder()
                        .id(restaurant3.getId())
                        .name(restaurant3.getName())
                        .updatedAt(dateTimeHelper.nowEpochMilli())
                        .build()
        );
        verify(key3, times(1)).getId();
        verify(result, times(1)).now();
    }

    @Test
    public void save_Update() {
        Saver saver = mock(Saver.class);
        when(objectify.save()).thenReturn(saver);
        Result result = mock(Result.class);
        RestaurantEntity Restaurant1_1 = RestaurantEntity.builder()
                .id(restaurant1.getId())
                .name("Restaurant 1.1")
                .updatedAt(dateTimeHelper.nowEpochMilli())
                .build();
        when(saver.entity(Restaurant1_1)).thenReturn(result);
        when(result.now()).thenReturn(key1);
        long id = restaurantRepository.save(Restaurant1_1);
        assertEquals(id1, id);
        verify(objectify, times(1)).save();
        verify(saver, times(1)).entity(Restaurant1_1);
        verify(key1, times(1)).getId();
        verify(result, times(1)).now();
    }

    @Test
    public void delete() {
        when(menuItemRepository.findAllByRestaurantId(id1)).thenReturn(
                Arrays.asList(
                        MenuItemEntity.builder().id(1l).name("Menu Item 1").price(10).restaurantId(id1).build(),
                        MenuItemEntity.builder().id(2l).name("Menu Item 1").price(20).restaurantId(id1).build()
                )
        );
        Deleter deleter = mock(Deleter.class);
        when(objectify.delete()).thenReturn(deleter);
        DeleteType deleteType = mock(DeleteType.class);
        Result result = mock(Result.class);
        when(deleteType.id(id1)).thenReturn(result);
        when(deleter.type(getType())).thenReturn(deleteType);
        restaurantRepository.delete(id1);
        verify(menuItemRepository, times(1)).findAllByRestaurantId(id1);
        verify(menuItemRepository, times(2)).delete(anyLong());
        verify(objectify, times(1)).delete();
        verify(deleter, times(1)).type(getType());
        verify(deleteType, times(1)).id(id1);
        verify(result, times(1)).now();
    }

}