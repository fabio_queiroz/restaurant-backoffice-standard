package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.StructuredQuery;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.LoadResult;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.cmd.DeleteType;
import com.googlecode.objectify.cmd.Deleter;
import com.googlecode.objectify.cmd.Query;
import com.googlecode.objectify.cmd.Saver;
import com.restaurant.backoffice.datastore.entity.AbstractEntity;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.restaurant.backoffice.datastore.entity.MenuItemEntity.MenuItemFields;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MenuItemObjectifyRepositoryTest extends AbstractObjectifyRepositoryTest {

    private static final StructuredQuery.OrderBy orderByName =
            StructuredQuery.OrderBy.asc(MenuItemFields.name.name());

    private final long restaurantId1 = 1;
    private final long id1 = 1;
    private final long id2 = 2;

    private MenuItemEntity menuItem1;
    private MenuItemEntity menuItem2;

    private MenuItemObjectifyRepository menuItemRepository;

    @Mock
    private LoadResult loadResult1;

    @Mock
    private Key key1;

    @Override
    protected Class<? extends AbstractEntity> getType() {
        return MenuItemEntity.class;
    }

    public void before() {
        menuItemRepository = new MenuItemObjectifyRepository(objectifyServiceFactory, dateTimeHelper);
        menuItem1 = MenuItemEntity.builder()
                .id(id1)
                .name("Item 1")
                .price(10.50)
                .restaurantId(restaurantId1)
                .build();
        menuItem2 = MenuItemEntity.builder()
                .id(id2)
                .name("Item 2")
                .price(20.50)
                .restaurantId(restaurantId1)
                .build();
        when(loadType.id(id1)).thenReturn(loadResult1);
        when(key1.getId()).thenReturn(id1);
    }

    public void after() {

    }

    @Test
    public void findById_Not_Found() {
        Optional<MenuItemEntity> menuItem = menuItemRepository.findById(id1);
        assertFalse(menuItem.isPresent());
    }

    @Test
    public void findById() {
        when(loadResult1.now()).thenReturn(menuItem1);
        Optional<MenuItemEntity> restaurantOpt = menuItemRepository.findById(id1);
        verify(objectifyServiceFactory, times(1)).ofy();
        verify(loader, times(1)).type(getType());
        assertEquals(menuItem1, restaurantOpt.get());
    }

    @Test
    public void findAll() {
        List<MenuItemEntity> menuItems = Arrays.asList(menuItem1, menuItem2);
        when(loadType.list()).thenReturn(menuItems);
        List<MenuItemEntity> menuItems2 = menuItemRepository.findAll();
        assertEquals(menuItems, menuItems2);
        verify(loader, times(1)).type(getType());
        verify(loadType, times(1)).list();
    }

    @Test
    public void findAll_Sorted() {
        List<MenuItemEntity> menuItems = Arrays.asList(menuItem1, menuItem2);
        Query query = mock(Query.class);
        when(loadType.order(orderByName.getProperty())).thenReturn(query);
        when(query.list()).thenReturn(menuItems);
        List<MenuItemEntity> menuItems2 = menuItemRepository.findAll(orderByName);
        assertEquals(menuItems, menuItems2);
        verify(loader, times(1)).type(getType());
        verify(loadType, times(1)).order(orderByName.getProperty());
        verify(query, times(1)).list();
    }

    @Test
    public void findAllByRestaurantId() {
        long restaurantId = 1l;
        List<MenuItemEntity> menuItems = Arrays.asList(menuItem1, menuItem2);
        Query query = mock(Query.class);
        when(loadType.filter(MenuItemFields.restaurantId.name(), restaurantId)).thenReturn(query);
        when(query.list()).thenReturn(menuItems);
        List<MenuItemEntity> menuItems2 = menuItemRepository.findAllByRestaurantId(restaurantId);
        assertEquals(menuItems, menuItems2);
        verify(loader, times(1)).type(getType());
        verify(loadType, times(1)).filter(MenuItemFields.restaurantId.name(), restaurantId);
        verify(query, times(1)).list();
    }

    @Test
    public void findAllByRestaurantId_Sorted() {
        List<MenuItemEntity> menuItems = Arrays.asList(menuItem1, menuItem2);
        Query query = mock(Query.class);
        when(loadType.filter(MenuItemFields.restaurantId.name(), restaurantId1)).thenReturn(query);
        when(query.order(orderByName.getProperty())).thenReturn(query);
        when(query.list()).thenReturn(menuItems);
        List<MenuItemEntity> menuItems2 = menuItemRepository.findAllByRestaurantId(restaurantId1, orderByName);
        assertEquals(menuItems, menuItems2);
        verify(loader, times(1)).type(getType());
        verify(loadType, times(1)).filter(MenuItemFields.restaurantId.name(), restaurantId1);
        verify(query, times(1)).order(orderByName.getProperty());
        verify(query, times(1)).list();
    }

    @Test
    public void save_Create() {
        long id3 = 3;
        MenuItemEntity menuItem3 = MenuItemEntity.builder()
                .name("Item 3")
                .price(30)
                .restaurantId(restaurantId1)
                .build();
        Saver saver = mock(Saver.class);
        when(objectify.save()).thenReturn(saver);
        Result result = mock(Result.class);
        when(saver.entity(
                MenuItemEntity.builder()
                        .name(menuItem3.getName())
                        .price(menuItem3.getPrice())
                        .restaurantId(restaurantId1)
                        .updatedAt(dateTimeHelper.nowEpochMilli())
                        .build()
        )).thenReturn(result);
        Key key3 = mock(Key.class);
        when(key3.getId()).thenReturn(id3);
        when(result.now()).thenReturn(key3);
        long id = menuItemRepository.save(menuItem3);
        assertEquals(id3, id);
        verify(objectify, times(1)).save();
        verify(saver, times(1)).entity(
                MenuItemEntity.builder()
                        .name(menuItem3.getName())
                        .price(menuItem3.getPrice())
                        .restaurantId(restaurantId1)
                        .updatedAt(dateTimeHelper.nowEpochMilli())
                        .build()
        );
        verify(key3, times(1)).getId();
        verify(result, times(1)).now();
    }

    @Test
    public void save_Update() {
        Saver saver = mock(Saver.class);
        when(objectify.save()).thenReturn(saver);
        Result result = mock(Result.class);
        MenuItemEntity menuItem1_1 = MenuItemEntity.builder()
                .id(menuItem1.getId())
                .name("Menu Item 1.1")
                .price(menuItem1.getPrice())
                .restaurantId(restaurantId1)
                .updatedAt(dateTimeHelper.nowEpochMilli())
                .build();
        when(saver.entity(menuItem1_1)).thenReturn(result);
        when(result.now()).thenReturn(key1);
        long id = menuItemRepository.save(menuItem1_1);
        assertEquals(id1, id);
        verify(objectify, times(1)).save();
        verify(saver, times(1)).entity(menuItem1_1);
        verify(key1, times(1)).getId();
        verify(result, times(1)).now();
    }

    @Test
    public void delete() {
        Deleter deleter = mock(Deleter.class);
        when(objectify.delete()).thenReturn(deleter);
        DeleteType deleteType = mock(DeleteType.class);
        Result result = mock(Result.class);
        when(deleteType.id(id1)).thenReturn(result);
        when(deleter.type(getType())).thenReturn(deleteType);
        menuItemRepository.delete(id1);
        verify(objectify, times(1)).delete();
        verify(deleter, times(1)).type(getType());
        verify(deleteType, times(1)).id(id1);
        verify(result, times(1)).now();
    }

}