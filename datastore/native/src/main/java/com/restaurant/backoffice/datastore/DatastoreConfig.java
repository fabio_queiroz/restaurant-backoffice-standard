package com.restaurant.backoffice.datastore;


import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.googlecode.objectify.ObjectifyService;
import com.restaurant.backoffice.common.DateTimeHelper;
import com.restaurant.backoffice.datastore.repository.MenuItemDatastoreRepository;
import com.restaurant.backoffice.datastore.repository.RestaurantDatastoreRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;

/**
 * Datastore Configuration
 */
@Profile("datastore")
@Configuration
@Slf4j
public class DatastoreConfig {

    /**
     * Initializing Objectify
     */
    @PostConstruct
    private void init() {
        log.info("Initializing Datastore");
        ObjectifyService.init();
    }

    /**
     * Defining Datastore bean
     *
     * @return Datastore bean
     */
    @Bean
    public Datastore datastore() {
        return DatastoreOptions.getDefaultInstance().getService();
    }

    /**
     * Defining RestaurantDatastoreRepository bean
     *
     * @param datastore Datastore
     * @param menuItemDatastoreRepository Menu Item datastore repository
     * @param dateTimeHelper Date time helper
     *
     * @return RestaurantDatastoreRepository bean
     */
    @Bean
    public RestaurantDatastoreRepository restaurantDatastoreRepository(
            Datastore datastore,
            MenuItemDatastoreRepository menuItemDatastoreRepository,
            DateTimeHelper dateTimeHelper) {
        return new RestaurantDatastoreRepository(datastore, menuItemDatastoreRepository, dateTimeHelper);
    }

    /**
     * Defining MenuItemDatastoreRepository bean
     *
     * @param datastore Datastore
     * @param dateTimeHelper Date time helper
     *
     * @return MenuItemDatastoreRepository bean
     */
    @Bean
    public MenuItemDatastoreRepository menuItemDatastoreRepository(Datastore datastore, DateTimeHelper dateTimeHelper) {
        return new MenuItemDatastoreRepository(datastore, dateTimeHelper);
    }

}
