package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.EntityQuery;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.KeyFactory;
import com.google.cloud.datastore.Query;
import com.google.cloud.datastore.QueryResults;
import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.AbstractEntity;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Abstract repository implementation using Datastore API
 */
public abstract class AbstractEntityDatastoreRepository<T extends AbstractEntity> implements EntityRepository<T> {

    /** Datastore */
    protected final Datastore datastore;

    /** Key factory */
    protected final KeyFactory keyFactory;

    /** Kind name */
    protected final String kind;

    /**
     * Default constructor
     *
     * @param datastore Datastore
     */
    public AbstractEntityDatastoreRepository(Datastore datastore) {
        this.datastore = datastore;
        kind = ((Class) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0]).getSimpleName().replaceAll("Entity", "");
        this.keyFactory = datastore.newKeyFactory().setKind(kind);
    }

    /**
     * Create entity bean
     *
     * @param entity Datastore entity
     * @return Entity bean
     */
    protected abstract T newEntityBean(Entity entity);

    /**
     * Create entity Datastore
     *
     * @param entityBean Entity bean
     * @return Datastore entity
     */
    protected abstract Entity newEntity(T entityBean);

    /**
     * Create a new key
     *
     * @param id Entity id
     * @return Key
     */
    protected Key key(Long id) {
        return id == null? datastore.allocateId(keyFactory.newKey()): keyFactory.newKey(id);
    }

    /**
     * Create a query builder
     *
     * @return Query builder
     */
    protected EntityQuery.Builder queryBuilder() {
        return Query.newEntityQueryBuilder().setKind(kind);
    }

    /**
     * Create a query builder
     *
     * @param orderBy Order by
     * @return Query builder
     */
    protected EntityQuery.Builder queryBuilder(StructuredQuery.OrderBy orderBy) {
        return queryBuilder().setOrderBy(orderBy);
    }

    /**
     * Run a query
     *
     * @return List of entity beans
     */
    protected List<T> runQuery(EntityQuery.Builder builder) {
        List<T> entityBeans = new ArrayList<>();
        QueryResults<Entity> entities = datastore.run(builder.build());
        while (entities.hasNext()) {
            Entity entity = entities.next();
            entityBeans.add(newEntityBean(entity));
        }
        return entityBeans;
    }

    /**
     * Find entity by id
     *
     * @param id Entity id
     * @return Entity
     */
    @Override
    public Optional<T> findById(long id) {
        Optional<Entity> entity = Optional.ofNullable(datastore.get(keyFactory.newKey(id)));
        return entity.map(this::newEntityBean);
    }

    /**
     * Find all entities
     *
     * @return List of entities
     */
    @Override
    public List<T> findAll() {
        return runQuery(queryBuilder());
    }

    /**
     * Find all entities sorted
     *
     * @param orderBy Order by
     *
     * @return List of entities
     */
    @Override
    public List<T> findAll(StructuredQuery.OrderBy orderBy) {
        return runQuery(queryBuilder(orderBy));
    }

    /**
     * Save a entity
     *
     * @param entityBean Entity bean
     * @return Entity id
     */
    @Override
    public long save(T entityBean) {
        Entity entity = newEntity(entityBean);
        datastore.put(entity);
        return entity.getKey().getId();
    }

    /**
     * Delete entity
     *
     * @param id Entity id
     */
    @Override
    public void delete(long id) {
        datastore.delete(keyFactory.newKey(id));
    }

}