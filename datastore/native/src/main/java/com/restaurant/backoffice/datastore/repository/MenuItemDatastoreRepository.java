package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.LongValue;
import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.common.DateTimeHelper;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;

import java.util.List;

import static com.restaurant.backoffice.datastore.entity.MenuItemEntity.MenuItemFields;

/**
 * Menu Item repository implementation using Datastore API
 */
public class MenuItemDatastoreRepository extends AbstractEntityDatastoreRepository<MenuItemEntity>
        implements MenuItemRepository {

    /** Date time helper */
    private final DateTimeHelper dateTimeHelper;

    /**
     * Default constructor
     *
     * @param datastore Datastore
     * @param dateTimeHelper Date time helper
     */
    public MenuItemDatastoreRepository(Datastore datastore, DateTimeHelper dateTimeHelper) {
        super(datastore);
        this.dateTimeHelper = dateTimeHelper;
    }

    /**
     * Creates a Menu Item bean
     * Set updatedAt attribute with current instant millis
     *
     * @param entity Entity
     * @return Menu Item entity
     */
    protected MenuItemEntity newEntityBean(Entity entity) {
        return MenuItemEntity.builder()
                .id(entity.getKey().getId())
                .name(entity.getString(MenuItemFields.name.name()))
                .price(entity.getDouble(MenuItemFields.price.name()))
                .restaurantId(entity.getLong(MenuItemFields.restaurantId.name()))
                .updatedAt(entity.contains(MenuItemFields.updatedAt.name())?
                        entity.getLong(MenuItemFields.updatedAt.name()): null)
                .build();
    }

    /**
     * Creates Datastore entity
     *
     * @param menuItem Menu Item entity
     * @return Datastore entity
     */
    @Override
    protected Entity newEntity(MenuItemEntity menuItem) {
        return Entity.newBuilder(key(menuItem.getId()))
                .set(MenuItemFields.name.name(), menuItem.getName())
                .set(MenuItemFields.price.name(), menuItem.getPrice())
                .set(MenuItemFields.restaurantId.name(), menuItem.getRestaurantId())
                // updatedAt is not indexed
                .set(MenuItemFields.updatedAt.name(), LongValue.newBuilder(dateTimeHelper.nowEpochMilli()).setExcludeFromIndexes(true).build())
                .build();
    }

    /**
     * Find all Menu Items of a Restaurant
     *
     * @return List of Menu Items
     */
    @Override
    public List<MenuItemEntity> findAllByRestaurantId(long restaurantId) {
        return runQuery(
            queryBuilder()
            .setFilter(
                StructuredQuery.CompositeFilter.and(
                    StructuredQuery.PropertyFilter.eq(MenuItemFields.restaurantId.name(), restaurantId)
                )
            )
        );
    }

    /**
     * Find all Menu Items of a Restaurant sorted
     *
     * @param restaurantId Restaurant id
     * @param orderBy Order by
     *
     * @return List of Menu Items
     */
    @Override
    public List<MenuItemEntity> findAllByRestaurantId(long restaurantId, StructuredQuery.OrderBy orderBy) {
        return runQuery(
            queryBuilder(orderBy)
            .setFilter(
                StructuredQuery.CompositeFilter.and(
                    StructuredQuery.PropertyFilter.eq(MenuItemFields.restaurantId.name(), restaurantId)
                )
            )
        );
    }

}
