package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.LongValue;
import com.restaurant.backoffice.common.DateTimeHelper;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;
import com.restaurant.backoffice.datastore.entity.RestaurantEntity;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

import static com.restaurant.backoffice.datastore.entity.RestaurantEntity.RestaurantFields;

/**
 * Restaurant repository implementation using Datastore API
 */
@Slf4j
public class RestaurantDatastoreRepository extends AbstractEntityDatastoreRepository<RestaurantEntity>
        implements RestaurantRepository {

    /** Menu Item datastore repository */
    private final MenuItemDatastoreRepository menuItemDatastoreRepository;

    /** Date time helper */
    private final DateTimeHelper dateTimeHelper;

    /**
     * Default constructor
     *
     * @param datastore Datastore
     * @param menuItemDatastoreRepository Menu Item repository
     * @param dateTimeHelper Date time helper
     */
    public RestaurantDatastoreRepository(
            Datastore datastore,
            MenuItemDatastoreRepository menuItemDatastoreRepository,
            DateTimeHelper dateTimeHelper) {
        super(datastore);
        this.menuItemDatastoreRepository = menuItemDatastoreRepository;
        this.dateTimeHelper = dateTimeHelper;
    }

    @Override
    public Optional<RestaurantEntity> findById(long id) {
        log.info("Finding restaurant by id {} on Datastore", id);
        return super.findById(id);
    }

    /**
     * Creates a restaurant bean
     * Set updatedAt attribute with current instant millis
     *
     * @param entity Entity
     * @return Restaurant entity
     */
    protected RestaurantEntity newEntityBean(Entity entity) {
        return RestaurantEntity.builder()
                .id(entity.getKey().getId())
                .name(entity.getString(RestaurantFields.name.name()))
                .updatedAt(entity.contains(RestaurantFields.updatedAt.name())?
                        entity.getLong(RestaurantFields.updatedAt.name()): null)
                .build();
    }

    /**
     * Creates Datastore entity
     *
     * @param restaurant Restaurant entity
     * @return Datastore entity
     */
    @Override
    protected Entity newEntity(RestaurantEntity restaurant) {
        return Entity.newBuilder(key(restaurant.getId()))
                .set(RestaurantFields.name.name(), restaurant.getName())
                // updatedAt is not indexed
                .set(RestaurantFields.updatedAt.name(), LongValue.newBuilder(dateTimeHelper.nowEpochMilli()).setExcludeFromIndexes(true).build())
                .build();
    }

    /**
     * Delete restaurant and its menu items
     *
     * @param restaurantId Restaurant id
     */
    @Override
    public void delete(long restaurantId) {
        menuItemDatastoreRepository.findAllByRestaurantId(restaurantId).stream()
                .mapToLong(MenuItemEntity::getId).forEach(menuItemDatastoreRepository::delete);
        super.delete(restaurantId);
    }

}
