package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.LongValue;
import com.google.cloud.datastore.QueryResults;
import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.MenuItemEntity;
import com.restaurant.backoffice.datastore.entity.RestaurantEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.restaurant.backoffice.datastore.entity.RestaurantEntity.RestaurantFields;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantDatastoreRepositoryTest extends AbstractDatastoreRepositoryTest {

    private static final StructuredQuery.OrderBy orderByName =
            StructuredQuery.OrderBy.asc(RestaurantFields.name.name());

    private final long id1 = 1;
    private final long id2 = 2;

    private RestaurantEntity restaurant1;
    private RestaurantEntity restaurant2;
    private Entity entity1;
    private Entity entity2;

    private RestaurantDatastoreRepository restaurantRepository;

    @Mock
    private Key key1;

    @Mock
    private Key key2;

    @Mock
    private MenuItemDatastoreRepository menuItemRepository;

    @Override
    protected String getKind() {
        return "Restaurant";
    }

    public void before() {
        when(keyFactory.newKey(id1)).thenReturn(key1);
        when(key1.getId()).thenReturn(id1);
        when(key2.getId()).thenReturn(id2);
        restaurantRepository = new RestaurantDatastoreRepository(datastore, menuItemRepository, dateTimeHelper);
        restaurant1 = RestaurantEntity.builder()
                .id(id1)
                .name("Restaurant 1")
                .build();
        entity1 = Entity.newBuilder(key1)
                .set(RestaurantFields.name.name(), restaurant1.getName())
                .build();
        restaurant2 = RestaurantEntity.builder()
                .id(id2)
                .name("Restaurant 2")
                .build();
        entity2 = Entity.newBuilder(key2)
                .set(RestaurantFields.name.name(), restaurant2.getName())
                .build();
    }

    public void after() {

    }

    @Test
    public void findById_Not_Found() {
        Optional<RestaurantEntity> restaurant = restaurantRepository.findById(id1);
        assertFalse(restaurant.isPresent());
    }

    @Test
    public void findById() {
        when(datastore.get(key1)).thenReturn(entity1);
        Optional<RestaurantEntity> restaurantOpt = restaurantRepository.findById(id1);
        verify(keyFactory, times(1)).newKey(id1);
        verify(datastore, times(1)).get(key1);
        assertEquals(restaurant1, restaurantOpt.get());
    }

    @Test
    public void findAll() {
        QueryResults<Entity> queryResults = mock(QueryResults.class);
        when(queryResults.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(queryResults.next()).thenReturn(entity2).thenReturn(entity1);
        when(datastore.run(restaurantRepository.queryBuilder().build())).thenReturn(queryResults);
        List<RestaurantEntity> restaurants = restaurantRepository.findAll();
        assertEquals(Arrays.asList(restaurant2, restaurant1), restaurants);
        verify(datastore, times(1)).run(restaurantRepository.queryBuilder().build());
    }

    @Test
    public void findAll_Sorted() {
        QueryResults<Entity> queryResults = mock(QueryResults.class);
        when(queryResults.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(queryResults.next()).thenReturn(entity1, entity2);
        when(datastore.run(restaurantRepository.queryBuilder(orderByName).build())).thenReturn(queryResults);
        List<RestaurantEntity> restaurants = restaurantRepository.findAll(orderByName);
        assertEquals(Arrays.asList(restaurant1, restaurant2), restaurants);
        verify(datastore, times(1)).run(restaurantRepository.queryBuilder(orderByName).build());
    }

    @Test
    public void save_Create() {
        long id3 = 3;
        RestaurantEntity restaurant3 = RestaurantEntity.builder()
                .name("Restaurant 3")
                .build();
        Key key3 = mock(Key.class);
        when(key3.getId()).thenReturn(id3);
        when(keyFactory.newKey()).thenReturn(key3);
        when(datastore.allocateId(key3)).thenReturn(key3);
        long id = restaurantRepository.save(restaurant3);
        assertEquals(id3, id);
        verify(keyFactory, times(1)).newKey();
        verify(datastore, times(1)).allocateId(key3);
        verify(datastore, times(1)).put(
                Entity.newBuilder(key3)
                        .set(RestaurantFields.name.name(), restaurant3.getName())
                        .set(RestaurantFields.updatedAt.name(), LongValue.newBuilder(dateTimeHelper.nowEpochMilli()).setExcludeFromIndexes(true).build())
                        .build()
        );
    }

    @Test
    public void save_Update() {
        RestaurantEntity restaurant1_1 = RestaurantEntity.builder()
                .id(id1)
                .name("Restaurant 1_1")
                .build();
        when(keyFactory.newKey(id1)).thenReturn(key1);
        long id = restaurantRepository.save(restaurant1_1);
        assertEquals(id1, id);
        verify(keyFactory, times(1)).newKey(id1);
        verify(datastore, times(1)).put(
                Entity.newBuilder(key1)
                        .set(RestaurantFields.name.name(), restaurant1_1.getName())
                        .set(RestaurantFields.updatedAt.name(), LongValue.newBuilder(dateTimeHelper.nowEpochMilli()).setExcludeFromIndexes(true).build())
                        .build()
        );
    }

    @Test
    public void delete() {
        when(menuItemRepository.findAllByRestaurantId(id1)).thenReturn(
                Arrays.asList(
                        MenuItemEntity.builder().id(1l).name("Menu Item 1").price(10).restaurantId(id1).build(),
                        MenuItemEntity.builder().id(2l).name("Menu Item 1").price(20).restaurantId(id1).build()
                )
        );
        restaurantRepository.delete(id1);
        verify(menuItemRepository, times(1)).findAllByRestaurantId(id1);
        verify(menuItemRepository, times(2)).delete(anyLong());
        verify(keyFactory, times(1)).newKey(id1);
        verify(datastore, times(1)).delete(key1);
    }

}