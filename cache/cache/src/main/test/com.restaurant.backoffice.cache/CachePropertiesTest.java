package com.restaurant.backoffice.cache;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CachePropertiesTest {

    private CacheProperties cacheProperties;

    @Mock
    private Environment env;

    @Before
    public void setUp() {
        cacheProperties = new CacheProperties(env);
    }

    @Test
    public void onApplicationEvent_Key_Not_Present() {
        String propertyKey = "restaurant-backoffice.cache.mycache.expiration.delta";
        final int expirationDeltaExpected = 3600;
        when(env.getProperty(propertyKey)).thenReturn(String.valueOf(expirationDeltaExpected));
        int expirationDelta = cacheProperties.getExpirationDelta("MyCache");
        verify(env, times(1)).getProperty(propertyKey);
        assertEquals(expirationDeltaExpected, expirationDelta);
    }


}