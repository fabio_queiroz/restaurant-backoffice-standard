package com.restaurant.backoffice.cache.event;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleValueWrapper;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CacheEventListenerTest {

    public static final String CACHE_NAME = "MyCache";

    public static final long KEY_1 = 1;
    public static final String VALUE_1 = "value1";

    private CacheEventListener cacheEventListener;

    @Mock
    private CacheManager cacheManager;

    @Mock
    private Cache cache;

    @Before
    public void setUp() {
        cacheEventListener = new CacheEventListener(cacheManager, CACHE_NAME);
        when(cacheManager.getCache(CACHE_NAME)).thenReturn(cache);
    }

    @Test
    public void onApplicationEvent_Key_Not_Present() {
        cacheEventListener.onApplicationEvent(
                CachePutEvent.builder()
                        .key(KEY_1)
                        .value(VALUE_1)
                        .source(this)
                        .build()
        );
        verify(cache, times(1)).get(KEY_1);
    }

    @Test
    public void onApplicationEvent_Put_Event_Key_Present() {
        when(cache.get(KEY_1)).thenReturn(new SimpleValueWrapper(VALUE_1));
        cacheEventListener.onApplicationEvent(
                CachePutEvent.builder()
                        .key(KEY_1)
                        .value(VALUE_1)
                        .source(this)
                        .build()
        );
        verify(cache, times(1)).get(KEY_1);
        verify(cache, times(1)).put(KEY_1, VALUE_1);
    }

    @Test
    public void onApplicationEvent_Remove_Event_Key_Present() {
        when(cache.get(KEY_1)).thenReturn(new SimpleValueWrapper(VALUE_1));
        cacheEventListener.onApplicationEvent(
                CacheRemoveEvent.builder()
                        .key(KEY_1)
                        .value(VALUE_1)
                        .source(this)
                        .build()
        );
        verify(cache, times(1)).get(KEY_1);
        verify(cache, times(1)).evict(KEY_1);
    }


}