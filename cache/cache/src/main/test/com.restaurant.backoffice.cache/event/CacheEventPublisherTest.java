package com.restaurant.backoffice.cache.event;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CacheEventPublisherTest {

    public static final long KEY_1 = 1;
    public static final String VALUE_1 = "value1";

    private CacheEventPublisher cacheEventPublisher;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Before
    public void setUp() {
        cacheEventPublisher = new CacheEventPublisher(applicationEventPublisher);
    }

    @Test
    public void publishEvent() {
        CacheRemoveEvent event = CacheRemoveEvent.builder()
                .key(KEY_1)
                .value(VALUE_1)
                .source(this)
                .build();
        cacheEventPublisher.publishEvent(event);
        verify(applicationEventPublisher, times(1)).publishEvent(event);
    }

    @Test
    public void publishPutEvent() {
        CachePutEvent event = CachePutEvent.builder()
                .key(KEY_1)
                .value(VALUE_1)
                .source(this)
                .build();
        cacheEventPublisher.publishPutEvent(this, KEY_1, VALUE_1);
        verify(applicationEventPublisher, times(1)).publishEvent(event);
    }

    @Test
    public void publishRemoveEvent() {
        CacheRemoveEvent event = CacheRemoveEvent.builder()
                .key(KEY_1)
                .value(VALUE_1)
                .source(this)
                .build();
        cacheEventPublisher.publishRemoveEvent(this, KEY_1, VALUE_1);
        verify(applicationEventPublisher, times(1)).publishEvent(event);
    }


}