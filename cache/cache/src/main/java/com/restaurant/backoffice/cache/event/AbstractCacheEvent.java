package com.restaurant.backoffice.cache.event;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/**
 * Base event triggered when a cached element must be updated or removed
 */
@Getter
@ToString
public abstract class AbstractCacheEvent<T> extends ApplicationEvent {

    /** Cache key */
    private final Object key;
    /** Value to be updated/removed */
    private final T value;

    /**
     * Constructor
     *
     * @param source Source object that triggered this event
     * @param key Cache key
     * @param value Value to be updated/removed
     */
    public AbstractCacheEvent(Object source, Object key, T value) {
        super(source);
        this.key = key;
        this.value = value;
    }

}