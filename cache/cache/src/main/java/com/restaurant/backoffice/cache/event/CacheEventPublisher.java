package com.restaurant.backoffice.cache.event;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * Publish cached events
 */
@Component
@RequiredArgsConstructor
public class CacheEventPublisher {

    /** Spring event publisher */
    private final ApplicationEventPublisher applicationEventPublisher;

    /**
     * Publish a cache event
     *
     * @param event Cache event
     */
    public void publishEvent(AbstractCacheEvent<?> event) {
        applicationEventPublisher.publishEvent(event);
    }

    /**
     * Publish an event for put operation
     *
     * @param source Source object
     * @param key Cache key
     * @param value Cache value
     */
    public void publishPutEvent(Object source, Object key, Object value) {
        publishEvent(
                CachePutEvent.builder()
                        .source(source)
                        .key(key)
                        .value(value)
                        .build()
        );
    }

    /**
     * Publish a restaurant event for remove operation
     *
     * @param source Source object
     * @param key Cache key
     * @param value Cache value
     */
    public void publishRemoveEvent(Object source, Object key, Object value) {
        publishEvent(
                CacheRemoveEvent.builder()
                        .source(source)
                        .key(key)
                        .value(value)
                        .build()
        );
    }

}
