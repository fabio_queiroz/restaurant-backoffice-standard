package com.restaurant.backoffice.cache.event;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Event triggered when a cache element must be removed
 */
@EqualsAndHashCode
@ToString(callSuper = true)
public class CacheRemoveEvent<T> extends AbstractCacheEvent<T> {

    /**
     * Constructor
     *
     * @param source Source object that triggered this event
     * @param key Cache key
     * @param value Value to be removed
     */
    @Builder
    public CacheRemoveEvent(Object source, Object key, T value) {
        super(source, key, value);
    }

}