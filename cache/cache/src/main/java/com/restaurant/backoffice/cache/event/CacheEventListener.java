package com.restaurant.backoffice.cache.event;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationListener;

/**
 * Cache event listener
 */
@Slf4j
@RequiredArgsConstructor
public class CacheEventListener<T extends AbstractCacheEvent> implements ApplicationListener<T> {

    /** Cache manager implementation */
    private final CacheManager cacheManager;
    /** Cache name */
    private final String cacheName;

    /**
     * Handle a cache event
     *
     * @param event Cache event
     */
    @Override
    public void onApplicationEvent(AbstractCacheEvent event) {
        log.info("Receiving cache event: {}", event);
        Cache cache = cacheManager.getCache(cacheName);
        if (cache.get(event.getKey()) != null) {
            if (event instanceof CachePutEvent) {
                cache.put(event.getKey(), event.getValue());
            } else if (event instanceof CacheRemoveEvent) {
                cache.evict(event.getKey());
            }
        }
    }

}
