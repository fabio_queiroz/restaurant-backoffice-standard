package com.restaurant.backoffice.cache.event;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Event triggered when a cached element must be updated
 */
@EqualsAndHashCode
@ToString(callSuper = true)
public class CachePutEvent<T> extends AbstractCacheEvent<T> {

    /**
     * Constructor
     *
     * @param source Source object that triggered this event
     * @param key Cache key
     * @param value Value to be updated
     */
    @Builder
    public CachePutEvent(Object source, Object key, T value) {
        super(source, key, value);
    }

}