package com.restaurant.backoffice.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Cache property wrapper
 */
@Component
@Slf4j
public class CacheProperties {

    private final Environment env;

    /**
     * Constructor
     *
     * @param env Environment
     */
    public CacheProperties(Environment env) {
        this.env = env;
    }

    /**
     * Get expiration delta (in seconds) for a cache
     *
     * @param cacheName Cache name
     */
    public int getExpirationDelta(String cacheName) {
        return Integer.parseInt(env.getProperty(
                String.format(
                        "restaurant-backoffice.cache.%s.expiration.delta",
                        cacheName.toLowerCase())
                )
        );
    }

}
