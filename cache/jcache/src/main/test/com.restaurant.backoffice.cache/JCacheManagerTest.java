package com.restaurant.backoffice.cache;

import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import com.restaurant.backoffice.cache.CacheProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JCacheManagerTest {

    private JCacheManager jcacheManager;

    @Mock
    private CacheFactory cacheFactory;

    @Mock
    private CacheProperties cacheProperties;

    @Mock
    private Cache cache;

    @Before
    public void setUp() {
        jcacheManager = new JCacheManager(cacheFactory, cacheProperties);
    }

    @Test
    public void loadCaches() {
        assertTrue(jcacheManager.loadCaches().isEmpty());
    }

    @Test
    public void getMissingCache() throws CacheException {
        final int expirationDelta = 3600;
        final String cacheName = "MyCache";
        Map<Object, Object> properties = new HashMap<>();
        when(cacheProperties.getExpirationDelta(cacheName)).thenReturn(expirationDelta);
        properties.put(GCacheFactory.EXPIRATION_DELTA, expirationDelta);
        properties.put(GCacheFactory.NAMESPACE, cacheName);
        when(cacheFactory.createCache(properties)).thenReturn(cache);
        JCache jcache = jcacheManager.getMissingCache(cacheName);

        verify(cacheProperties, times(1)).getExpirationDelta(cacheName);
        verify(cacheFactory, times(1)).createCache(properties);

        assertEquals(cacheName, jcache.getName());
        assertEquals(cache, jcache.getNativeCache());
    }

}