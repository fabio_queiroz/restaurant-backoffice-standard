package com.restaurant.backoffice.cache;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.cache.Cache;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class JCacheTest {

    public static final String CACHE_NAME = "MyCache";

    public static final long KEY_1 = 1;
    public static final String VALUE_1 = "value1";

    private JCache jcache;

    @Mock
    private Cache cache;

    @Before
    public void setUp() {
        jcache = new JCache(CACHE_NAME, cache);
    }

    @Test
    public void getName() {
        assertEquals(jcache.getName(), CACHE_NAME);
    }

    @Test
    public void getNativeCache() {
        assertEquals(jcache.getNativeCache(), cache);
    }

    @Test
    public void get() {
        when(cache.get(KEY_1)).thenReturn(VALUE_1);
        Object value = jcache.get(KEY_1).get();
        verify(cache, times(1)).get(KEY_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void get_With_Type() {
        when(cache.get(KEY_1)).thenReturn(VALUE_1);
        String value = jcache.get(KEY_1, String.class);
        verify(cache, times(1)).get(KEY_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void get_With_Callable_Contains_Key() {
        when(cache.containsKey(KEY_1)).thenReturn(true);
        when(cache.get(KEY_1)).thenReturn(VALUE_1);
        String value = jcache.get(KEY_1, () -> VALUE_1);
        verify(cache, times(1)).containsKey(KEY_1);
        verify(cache, times(1)).get(KEY_1);
        verify(cache, never()).put(KEY_1, VALUE_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void get_With_Callable_Not_Contains_Key() {
        when(cache.containsKey(KEY_1)).thenReturn(false);
        String value = jcache.get(KEY_1, () -> VALUE_1);
        verify(cache, times(1)).containsKey(KEY_1);
        verify(cache, never()).get(KEY_1);
        verify(cache, times(1)).put(KEY_1, VALUE_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void put() {
        jcache.put(KEY_1, VALUE_1);
        verify(cache, times(1)).put(KEY_1, VALUE_1);
    }

    @Test
    public void putIfAbsent_Contains_Key() {
        when(cache.containsKey(KEY_1)).thenReturn(true);
        when(cache.get(KEY_1)).thenReturn(VALUE_1);
        Object value = jcache.putIfAbsent(KEY_1, VALUE_1).get();
        verify(cache, times(1)).containsKey(KEY_1);
        verify(cache, times(1)).get(KEY_1);
        verify(cache, never()).put(KEY_1, VALUE_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void putIfAbsent_Not_Contains_Key() {
        when(cache.containsKey(KEY_1)).thenReturn(false);
        Object value = jcache.putIfAbsent(KEY_1, VALUE_1).get();
        verify(cache, times(1)).containsKey(KEY_1);
        verify(cache, never()).get(KEY_1);
        verify(cache, times(1)).put(KEY_1, VALUE_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void evict() {
        jcache.evict(KEY_1);
        verify(cache, times(1)).remove(KEY_1);
    }

    @Test
    public void clear() {
        jcache.clear();
        verify(cache, times(1)).clear();
    }

}