package com.restaurant.backoffice.cache;

import com.google.appengine.api.memcache.stdimpl.GCacheFactory;
import com.restaurant.backoffice.cache.CacheProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;

import javax.cache.CacheException;
import javax.cache.CacheFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * JCache Manager
 */
@Slf4j
public class JCacheManager extends AbstractCacheManager {

    /** Internal cache names */
    private final Collection<? extends Cache> caches = Collections.emptySet();

    /** Cache factory */
    private final CacheFactory cacheFactory;

    /** Cache properties */
    private final CacheProperties cacheProperties;

    /**
     * Constructor
     *
     * @param cacheFactory Cache factory
     * @param cacheProperties Cache properties
     */
    public JCacheManager(CacheFactory cacheFactory, CacheProperties cacheProperties) {
        this.cacheFactory = cacheFactory;
        this.cacheProperties = cacheProperties;
    }

    /**
     * Load internal cache names
     * @return
     */
    @Override
    protected Collection<? extends Cache> loadCaches() {
        return this.caches;
    }

    /**
     * Get missing caches
     *
     * @param name Cache name
     * @return JCache instance
     */
    @Override
    protected JCache getMissingCache(String name) {
        log.info("Creating cache {}", name);
        try {
            Map<Object, Object> properties = new HashMap<>();
            properties.put(GCacheFactory.EXPIRATION_DELTA, cacheProperties.getExpirationDelta(name));
            properties.put(GCacheFactory.NAMESPACE, name);
            return new JCache(name, cacheFactory.createCache(properties));
        } catch (CacheException e) {
            throw new RuntimeException(e);
        }
    }

}
