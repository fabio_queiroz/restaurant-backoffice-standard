package com.restaurant.backoffice.cache;

import com.restaurant.backoffice.cache.CacheProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;


/**
 * JCache configuration
 */
@Profile("jcache")
@Configuration
@Slf4j
public class JCacheConfiguration {

    /**
     * Initializer method
     */
    @PostConstruct
    public void init() {
        log.info("Initializing JCacheConfiguration");
    }

    /**
     * Defining JCache manager
     *
     * @return JCache manager
     */
    @Bean
    JCacheManager cacheManager(CacheFactory cacheFactory, CacheProperties cacheProperties) {
        JCacheManager cacheManager = new JCacheManager(cacheFactory, cacheProperties);
        return cacheManager;
    }

    /**
     * Defining Cache factory
     *
     * @return Cache factory
     */
    @Bean
    public CacheFactory cacheFactory() throws CacheException {
        return CacheManager.getInstance().getCacheFactory();
    }

}