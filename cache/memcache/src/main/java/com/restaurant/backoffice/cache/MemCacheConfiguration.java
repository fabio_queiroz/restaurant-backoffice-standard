package com.restaurant.backoffice.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;


/**
 * MemCache configuration
 */
@Profile("memcache")
@Configuration
@Slf4j
public class MemCacheConfiguration {

    /**
     * Initializer method
     */
    @PostConstruct
    public void init() {
        log.info("Initializing MemCacheConfiguration");
    }

    /**
     * Defining MemCache manager
     *
     * @return MemCache manager
     */
    @Bean
    MemCacheManager cacheManager(MemCacheFactory memCacheFactory, CacheProperties cacheProperties) {
        MemCacheManager cacheManager = new MemCacheManager(memCacheFactory, cacheProperties);
        return cacheManager;
    }

    /**
     * MemCache Factory
     *
     * @return MemCacheFactory
     */
    @Bean
    MemCacheFactory memCacheFactory() {
        return new MemCacheFactory();
    }

}