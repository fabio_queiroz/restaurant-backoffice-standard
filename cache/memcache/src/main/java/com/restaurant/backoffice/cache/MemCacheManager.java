package com.restaurant.backoffice.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.support.AbstractCacheManager;

import java.util.Collection;
import java.util.Collections;

/**
 * Memcache Manager
 */
@Slf4j
public class MemCacheManager extends AbstractCacheManager {

    /** Internal cache names */
    private Collection<? extends Cache> caches = Collections.emptySet();

    /** MemCache Factory */
    private final MemCacheFactory memCacheFactory;

    /** Cache properties */
    private final CacheProperties cacheProperties;

    /**
     * Constructor
     *
     * @param memCacheFactory MemCache Factory
     * @param cacheProperties Cache properties
     */
    public MemCacheManager(MemCacheFactory memCacheFactory, CacheProperties cacheProperties) {
        this.memCacheFactory = memCacheFactory;
        this.cacheProperties = cacheProperties;
    }

    /**
     * Load internal cache names
     * @return
     */
    @Override
    protected Collection<? extends Cache> loadCaches() {
        return this.caches;
    }

    /**
     * Get missing caches
     *
     * @param name Cache name
     * @return MemCache instance
     */
    @Override
    protected MemCache getMissingCache(String name) {
        log.info("Creating cache {}", name);
        return new MemCache(
                name,
                this.memCacheFactory.getMemcacheService(name),
                cacheProperties.getExpirationDelta(name)
        );
    }

}
