package com.restaurant.backoffice.cache;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

/**
 * Memcache factory to abstract static implementation in favor of Unit Tests
 */
public class MemCacheFactory {

    /**
     * Get Memcache service
     *
     * @param namespace Cache namespace
     * @return Memcache service
     */
    public MemcacheService getMemcacheService(String namespace) {
        return MemcacheServiceFactory.getMemcacheService(namespace);
    }

}
