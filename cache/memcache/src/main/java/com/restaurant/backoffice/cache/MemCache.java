package com.restaurant.backoffice.cache;

import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;

import java.util.concurrent.Callable;

/**
 * Google Memcache implementation
 */
@Slf4j
public class MemCache implements Cache {

    /** Cache name */
    private final String name;

    /** Expiration delta in seconds */
    private final int expirationDelta;

    /** Memcache service */
    private final MemcacheService cache;

    /**
     * Memcache constructor
     *
     * @param name Cache name
     * @param memcacheService Memcache service
     * @param expirationDelta Expiration delta in seconds
     */
    public MemCache(String name, MemcacheService memcacheService, int expirationDelta) {
        this.name = name;
        this.cache = memcacheService;
        this.expirationDelta = expirationDelta;
    }

    /**
     * Get cache name
     *
     * @return Cache name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Get Native cache
     *
     * @return native cache
     */
    @Override
    public Object getNativeCache() {
        return cache;
    }

    /**
     * Get cache value
     *
     * @param key Cache key
     *
     * @return Cache value
     */
    @Override
    public ValueWrapper get(Object key) {
        Object value = cache.get(key);
        log.info("Getting key/value from cache {}/{}", key, value);
        return toValueWrapper(value);
    }

    /**
     * Get cache value
     *
     * @param key Key
     * @param type Type
     * @param <T> Class type
     *
     * @return Cache value
     */
    @Override
    public <T> T get(Object key, Class<T> type) {
        Object value = cache.get(key);
        log.info("Getting key/value from cache {}/{}", key, value);
        return (T) value;
    }

    /**
     * Get cache value
     *
     * @param key Key
     * @param valueLoader Value loader
     * @param <T> Class type
     *
     * @return Cache value
     */
    public <T> T get(Object key, Callable<T> valueLoader) {
        T value;
        if (this.cache.contains(key)) {
            value = (T) get(key).get();
        } else {
            try {
                value = valueLoader.call();
                cache.put(key, value);
            }
            catch (Throwable ex) {
                throw new ValueRetrievalException(key, valueLoader, ex);
            }
        }
        log.info("Getting key/value from cache {}/{}", key, value);
        return value;
    }

    /**
     * Put a key value pair on Memcache
     *
     * @param key Key
     * @param value Value
     */
    @Override
    public void put(final Object key, final Object value) {
        log.info("Putting key/value on cache {}/{}", key, value);
        cache.put(key, value, Expiration.byDeltaSeconds(expirationDelta));
    }

    /**
     * Put a key value pair on Memcache if absent
     *
     * @param key Key
     * @param value Value
     *
     * @return Cache value
     */
    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        log.info("Putting if absent key/value on cache {}/{}", key, value);
        ValueWrapper vw;
        if (this.cache.contains(key)) {
            vw = get(key);
        } else {
            put(key, value);
            vw = toValueWrapper(value);
        }
        return vw;
    }

    /**
     * Delete a key value pair from Cache
     *
     * @param key Key
     */
    @Override
    public void evict(final Object key) {
        log.info("Evicting key/value from cache {}", key);
        this.cache.delete(key);
    }

    /**
     * Clear cache elements
     */
    @Override
    public void clear() {
        log.info("Clearing cache {}", name);
        cache.clearAll();
    }

    /**
     * Value wrapper, returns null if value is null
     *
     * @param storeValue Value
     *
     * @return Value wrapper
     */
    private Cache.ValueWrapper toValueWrapper(Object storeValue) {
        return (storeValue != null ? new SimpleValueWrapper(storeValue) : null);
    }

}
