package com.restaurant.backoffice.cache;

import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MemCacheTest {

    public static final String CACHE_NAME = "MyCache";
    public static final int EXPIRATION_DELTA = 3600;
    public static final long KEY_1 = 1;
    public static final String VALUE_1 = "value1";

    private MemCache memCache;

    @Mock
    private MemcacheService memcacheService;

    @Before
    public void setUp() {
        memCache = new MemCache(CACHE_NAME, memcacheService, EXPIRATION_DELTA);
    }

    @Test
    public void getName() {
        assertEquals(memCache.getName(), CACHE_NAME);
    }

    @Test
    public void getNativeCache() {
        assertEquals(memCache.getNativeCache(), memcacheService);
    }

    @Test
    public void get() {
        when(memcacheService.get(KEY_1)).thenReturn(VALUE_1);
        Object value = memCache.get(KEY_1).get();
        verify(memcacheService, times(1)).get(KEY_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void get_With_Type() {
        when(memcacheService.get(KEY_1)).thenReturn(VALUE_1);
        String value = memCache.get(KEY_1, String.class);
        verify(memcacheService, times(1)).get(KEY_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void get_With_Callable_Contains_Key() {
        when(memcacheService.contains(KEY_1)).thenReturn(true);
        when(memcacheService.get(KEY_1)).thenReturn(VALUE_1);
        String value = memCache.get(KEY_1, () -> VALUE_1);
        verify(memcacheService, times(1)).contains(KEY_1);
        verify(memcacheService, times(1)).get(KEY_1);
        verify(memcacheService, never()).put(KEY_1, VALUE_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void get_With_Callable_Not_Contains_Key() {
        when(memcacheService.contains(KEY_1)).thenReturn(false);
        String value = memCache.get(KEY_1, () -> VALUE_1);
        verify(memcacheService, times(1)).contains(KEY_1);
        verify(memcacheService, never()).get(KEY_1);
        verify(memcacheService, times(1)).put(KEY_1, VALUE_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void put() {
        memCache.put(KEY_1, VALUE_1);
        verify(memcacheService, times(1)).put(
                KEY_1, VALUE_1, Expiration.byDeltaSeconds(EXPIRATION_DELTA));
    }

    @Test
    public void putIfAbsent_Contains_Key() {
        when(memcacheService.contains(KEY_1)).thenReturn(true);
        when(memcacheService.get(KEY_1)).thenReturn(VALUE_1);
        Object value = memCache.putIfAbsent(KEY_1, VALUE_1).get();
        verify(memcacheService, times(1)).contains(KEY_1);
        verify(memcacheService, times(1)).get(KEY_1);
        verify(memcacheService, never()).put(KEY_1, VALUE_1);
        assertEquals(VALUE_1, value);
    }

    @Test
    public void putIfAbsent_Not_Contains_Key() {
        when(memcacheService.contains(KEY_1)).thenReturn(false);
        Object value = memCache.putIfAbsent(KEY_1, VALUE_1).get();
        verify(memcacheService, times(1)).contains(KEY_1);
        verify(memcacheService, never()).get(KEY_1);
        verify(memcacheService, times(1)).put(
                KEY_1, VALUE_1, Expiration.byDeltaSeconds(EXPIRATION_DELTA));
        assertEquals(VALUE_1, value);
    }

    @Test
    public void evict() {
        memCache.evict(KEY_1);
        verify(memcacheService, times(1)).delete(KEY_1);
    }

    @Test
    public void clear() {
        memCache.clear();
        verify(memcacheService, times(1)).clearAll();
    }

}