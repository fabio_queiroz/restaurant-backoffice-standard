package com.restaurant.backoffice.cache;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class MemCacheFactoryTest {

    private MemCacheFactory memCacheFactory;

    @Before
    public void setUp() {
        memCacheFactory = new MemCacheFactory();
    }

    @Test
    public void getMemcacheService() {
        assertNotNull(memCacheFactory.getMemcacheService("MyNamespace"));
    }


}