package com.restaurant.backoffice.cache;

import com.google.appengine.api.memcache.MemcacheService;
import com.restaurant.backoffice.cache.CacheProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.cache.CacheException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MemCacheManagerTest {

    private MemCacheManager memCacheManager;

    @Mock
    private MemCacheFactory memCacheFactory;

    @Mock
    private CacheProperties cacheProperties;

    @Mock
    private MemcacheService memcacheService;

    @Before
    public void setUp() {
        memCacheManager = new MemCacheManager(memCacheFactory, cacheProperties);
    }

    @Test
    public void loadCaches() {
        assertTrue(memCacheManager.loadCaches().isEmpty());
    }

    @Test
    public void getMissingCache() throws CacheException {
        final int expirationDelta = 3600;
        final String cacheName = "MyCache";
        Map<Object, Object> properties = new HashMap<>();
        when(cacheProperties.getExpirationDelta(cacheName)).thenReturn(expirationDelta);
        when(memCacheFactory.getMemcacheService(cacheName)).thenReturn(memcacheService);
        MemCache memCache = memCacheManager.getMissingCache(cacheName);

        verify(cacheProperties, times(1)).getExpirationDelta(cacheName);
        verify(memCacheFactory, times(1)).getMemcacheService(cacheName);

        assertEquals(cacheName, memCache.getName());
        assertEquals(memcacheService, memCache.getNativeCache());
    }

}