package com.restaurant.backoffice.common;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Helper class to encapsulate Date API static methods to mock them during Unit Tests
 */
@Component
public class DateTimeHelper {

    public static final String PATTERN_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    public static final DateTimeFormatter DTF_YYYY_MM_DD_HH_MM_SS =
            DateTimeFormatter.ofPattern(PATTERN_YYYY_MM_DD_HH_MM_SS).withZone(ZoneId.systemDefault());

    /**
     * Now As Instant
     *
     * @return Now
     */
    public Instant nowAsInstant() {
        return Instant.now();
    }

    /**
     * Now epoch millis
     *
     * @return Epoch millis
     */
    public long nowEpochMilli() {
        return nowAsInstant().toEpochMilli();
    }

}
