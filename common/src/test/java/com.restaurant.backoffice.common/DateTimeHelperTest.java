package com.restaurant.backoffice.common;

import org.junit.Before;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DateTimeHelperTest {

    private DateTimeHelper dateTimeHelper;

    @Before
    public void setUp() {
        dateTimeHelper = new DateTimeHelper();
    }

    @Test
    public void testNowAsInstant() {
        assertNotNull(dateTimeHelper.nowAsInstant());
    }

    @Test
    public void testInstantOfEpochMilli() {
        assertTrue(Instant.now().toEpochMilli() <= dateTimeHelper.nowEpochMilli());
    }

}
